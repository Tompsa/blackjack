/**
   cardfuncs.h
   Purpose: header file for card related functions 
   @author Tommi Kilponen
   @version 0.1 7/2014
*/

#ifndef CARDFUNCS_H
#define CARDFUNCS_H

enum cardSuit {
  CLUBS,
  DIAMONDS,
  HEARTS,
  SPADES
};

typedef struct cardStruct Card;

struct cardStruct {
  enum cardSuit suit;
  short int value;
  short int index;
  Card *next;
};

typedef struct deckStruct Deck;

struct deckStruct {
  Card *head;
};

/**
   Creates a standard 52 card deck.
   @param deck A pointer to a Deck struct.
   @return void
 */
void createDeck(Deck *deck);

/**
   Inserts a new card after a card.
   @param card A pointer to card.
   @param newCard A pointer to a card to be inserted.
   @return void
 */
void insertCard(Card *card, Card *newCard);

/**
   Shuffles deck to random order
   @param deck A pointer to Deck struct.
   @return void
 */
void shuffleDeck(Deck *deck);

/**
   Get card from given index.
   @param head The first card in hand or deck.
   @param index The index of card to be returned.
   @return A pointer to Card struct.
 */
Card *scanCards(Card *head, int index);

/**
   Prints card suits and values on one line from given hand/deck.
   @param deck A pointer to hand/deck to be printed.
   @param index Amount of cards to print (0 prints all).
   @return void
 */
void printCards(Deck *deck, int index);

/**
   Deals a card from deck.
   @param deck A pointer to Deck struct.
   @param hand A pointer hand to which card is dealt.
   @return void
 */
void dealCard(Deck *deck, Deck *hand);

/**
   Calculates hand value and returns it 
   (no special handling for  aces yet, valued always as 1).
   @param hand A pointer to hand.
   @return Hand value.
 */
short int getHandValue(Deck *hand);

/**
   Free memory allocated to cards from given deck/hand.
   @param deck A pointer to deck/hand.
   @return void
 */
void cleanCards(Deck *deck);

#endif /* CARDFUNCS_H */
