/**
   gamelogic.c
   Purpose: main blackjack game logic and related functions
   @author Tommi Kilponen
   @version 0.1 7/2014
 */

#include <stdlib.h>
#include <stdio.h>
#include "cardfuncs.h"

// Returns value given by user based on param size
int handleInput(int size)
{
  char input[size];
  int value;
  while(1)
    {
      fgets(input, sizeof input, stdin);  
      int sscanfResult = sscanf(input, "%d", &value);
      if(sscanfResult == 0 || sscanfResult == EOF)	 
	printf("\nInput was non-integer!");     	  
      else
	return value;
    }
}

// Handles how the player bets credits
void betCredits(int *credits, int *bet)
{
  system("clear");
  while(1)
    {
      printf("You currently have %d credits.\n How much would you like to bet\n->", *credits);
      *bet = handleInput(7);
      if(*bet <= *credits && *bet > 0)
	{
	  *credits = *credits - *bet;
	  break;
	}
      else
	{
	  system("clear");
	  puts("Invalid bet!");
	}
    }
}

// Handles how player can hit for more cards or to stand and continue.
void dealUntilDone(Deck *deck, Deck *playerHand, Deck *dealerHand, int *bet, int *credits)
{
  while(1)
    {
      int action = 0;
      puts("You have following cards:");
      printCards(playerHand, 0);
      puts("\nDealer has following cards:");
      printCards(dealerHand, 1);
      puts("and another card face down on the table.");
      printf("Credits: %d", *credits);
      printf("\nChoose action: (1)Hit (2)Stand (3)Double down\n->");
      action = handleInput(3);
      if(action == 1)
	{
	  if(getHandValue(playerHand) < 21)
	    {
	      dealCard(deck, playerHand);
	      system("clear");
	    }
	  else
	    {
	      puts("Hand value too high");
	      break;
	    }
	}
      else if(action == 2)
	break;
      else if(action ==  3)
	{
	  if(*credits-*bet >= 0)
	    {
	      *bet = 2**bet;
	      *credits = *credits - *bet;
	      break;
	    }
	  else
	    puts("Insufficient funds!");
	}
      else
	printf("\nAre you ok? 1, 2 or 3 only!\n");
    } /* while() */
}

// Main blackjack game loop which is run in main
void gameLoop()
{
  int credits = 500;
  while(1)
    {
      system("clear");
      int bet = 0;
      int action = 0;
      Deck *deck = NULL;
      Deck *playerHand = NULL;
      Deck *dealerHand = NULL;
      deck = (Deck*) malloc(sizeof(Deck));
      playerHand = (Deck*) malloc(sizeof(Deck));
      playerHand->head = NULL;
      dealerHand = (Deck*) malloc(sizeof(Deck));
      dealerHand->head = NULL;
      createDeck(deck);
      shuffleDeck(deck);

      betCredits(&credits, &bet);
      for(short int i = 0; i < 2; i++)
	{
	  dealCard(deck, playerHand);
	  dealCard(deck, dealerHand);
	}
      system("clear");
      dealUntilDone(deck, playerHand, dealerHand, &bet, &credits);
      while(getHandValue(dealerHand) < 17)
	dealCard(deck, dealerHand);
      puts("Dealer has:");
      printCards(dealerHand, 0);
      short int dealerValue =  getHandValue(dealerHand);
      short int playerValue = getHandValue(playerHand);
      if(playerValue > dealerValue && playerValue <= 21)
	{
	  puts("\nYou win!");
	  credits += 2*bet;
	}
      else
	puts("\nYou lose...");
      while(1)
	{
	  printf("Keep playing? (1)Yes (2)No\n->");
	  action = handleInput(3); 
	  if(action == 1 || action == 2)
	    break;
	  else
	    puts("Invalid input");
	}
      cleanCards(deck);
      cleanCards(playerHand);
      cleanCards(dealerHand);
      free(deck);
      free(playerHand);
      free(dealerHand);
      system("clear");
      if(action == 2)
	break;
    }
  puts("Thank you for playing...");
}

int main(int argc, char *argv[])
{  
  gameLoop();
  return 0;
}
