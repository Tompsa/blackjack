//Blackjack on CLI

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

void createDeck(char *deck);

void shuffleDeck(char *deck);

void dealCard(char *deck, char *player, unsigned int *x, unsigned int *y);

void printCards(char *cards, unsigned int size, unsigned int index);

unsigned int betCredits(int *credits);

unsigned int getHandValue(char *cards, unsigned int size, unsigned int index);

int main(int argc, char *argv[])
{   while(1)
    {
        const unsigned int deckSize = 52;
        const unsigned int handSize = 6;
        unsigned int credits = 500;
        unsigned int bet = 0;
        unsigned int value = 0;
        int action = 0;
        unsigned int deckIndex = 0, playerIndex = 0, dealerIndex = 0;
        
        char *deck = NULL;
        deck = (char *)malloc(sizeof(char)*2*52);
        char *playerHand = NULL;
        playerHand = (char *)malloc(sizeof(char)*2*6);
        char *dealerHand = NULL;
        dealerHand = (char *)malloc(sizeof(char)*2*6);
        
        createDeck(deck);
        //printCards(deck, deckSize);
        
        shuffleDeck(deck);
        //printCards(deck, deckSize);
        
        bet = betCredits(&credits);
        
        for(unsigned int i = 0; i < 2; i++)
        {
            dealCard(deck, playerHand, &deckIndex, &playerIndex);
            dealCard(deck, dealerHand, &deckIndex, &dealerIndex);
        }
        
        while(1)
        {
            printf("You have:\n");
            printCards(playerHand, handSize, playerIndex);
            printf("\nDealer has:\n");
            printCards(dealerHand, handSize, dealerIndex-1);
            printf("and another card face down on table");
            printf("\nCredits: %i", credits);
            printf("\nChoose action\n(1)Hit (2)Stand (3)Double down\n->");
            scanf("%i", &action);
            if(action == 1)
            {
                dealCard(deck, playerHand, &deckIndex, &playerIndex);
                value = getHandValue(playerHand, handSize, playerIndex);
                if(value > 21)
                    break;
                else
                    continue;
            }
            else if(action == 2)
                break;
            else if(action == 3)
                if(2*bet > credits)
                {
                    printf("\nInsufficient credits.");
                    continue;
                }
                else
                {
                    bet = 2*bet;
                    credits = credits-2*bet;
                    break;
                }
            else
            {
                printf("Are you ok?");
                continue;
            }
        }
        
        while(17 > getHandValue(dealerHand, handSize, dealerIndex))
            dealCard(deck, dealerHand, &deckIndex, &dealerIndex);
        printf("\nDealer has:\n");
        printCards(dealerHand, handSize, dealerIndex);
        
        
        unsigned int dealerValue = getHandValue(dealerHand, handSize, dealerIndex);
        unsigned int playerValue = getHandValue(playerHand, handSize, dealerIndex);
        if(playerValue > dealerValue)
        {
            printf("\nYou win!");
            credits += 2*bet;
        }
        else
            printf("\nYou lose...");
        
        while(1)
        {
            printf("Keep playing?\n(1)Yes (2)No\n-> ");
            scanf(" %i", &action);
            if(action == 1 || action == 2)
                break;
            else
                printf("Invalid input");
        }
        
        free(deck);
        free(dealerHand);
        free(playerHand);
        if(action == 2) 
            break;
    }
    printf("Exiting...");
    return 0;
}

void createDeck(char *deck)
{
    puts("Creating a deck...");
    unsigned int suit = 0;
    unsigned int value = 1;
    for(unsigned int j = 0; j < 52; j++)
    {
        for(unsigned int i = 0; i < 2 ; i++)
        {
            if(i == 0)
            {
                switch(suit)
                {
                    case 0: 
                        deck[i*52 + j] = 'C';
                        break;
                    case 1:
                        deck[i*52 + j] = 'D';
                        break;
                    case 2:
                        deck[i*52 + j] = 'H';
                        break;
                    case 3:            
                        deck[i*52 + j] = 'S';
                        break;
                    default:
                        puts("Something went wrong...");
                        break;
                }  
            }
            else
            {
                switch(value)
                {
                    case 1: 
                        deck[i*52 + j] = 'A';
                        break;
                    case 10:
                        deck[i*52 + j] = 'T';
                        break;
                    case 11:
                        deck[i*52 + j] = 'J';
                        break;
                    case 12:            
                        deck[i*52 + j] = 'Q';
                        break;
                    case 13:            
                        deck[i*52 + j] = 'K';
                        break;
                    default:
                        deck[i*52 + j] = (char)(((int)'0')+value);
                        break;
                } 
            }         
        }
        if(value == 13)
        {
            value = 1;
            suit++;
        }
        else
            value++;
    }
    puts("Done.");
}

void shuffleDeck(char *deck)
{
    time_t timer;
    struct tm * ptm;
    
    time(&timer);
    
    ptm = gmtime(&timer);
    
    srand((unsigned int)ptm);
    
    puts("Shuffling deck...");
    for(unsigned int j = 0; j < 52; j++)
    {
        int r = j + (rand() % (52-j)); 
        char suit_temp = deck[0*52 + j];
        char value_temp = deck[1*52 + j];
        
        deck[0*52 + j] = deck[0*52 + r];
        deck[1*52 + j] = deck[1*52 + r];
        
        deck[0*52 + r] = suit_temp;
        deck[1*52 + r] = value_temp;
    }
    puts("Done.");
}

void dealCard(char *deck, char *player, unsigned int *x, unsigned int *y)
{   
    player[(0*6) + (*y)] = deck[(0*52) + (*x)]; 
    player[(1*6) + (*y)] = deck[(1*52) + (*x)]; 
    (*x)++;
    (*y)++;
}

void printCards(char *cards, unsigned int size, unsigned int index)
{
    for(unsigned int j=0; j < index; j++) {
        for(unsigned int i=0; i < 2; i++) {
            printf("%c", cards[(i * size) + j]);
        }
        printf(" ");
    }
}

unsigned int betCredits(int *credits)
{
    unsigned int bet;
    printf("You currently have %i credits. \nHow much would you like to bet?\n->", *credits);
    //tähän virheentarkistus
    scanf(" %i", &bet);  
    *credits = *credits - bet;
    return bet;
}

unsigned int getHandValue(char *cards, unsigned int size, unsigned int index)
{
    unsigned int handValue = 0;
    for(unsigned int j=0; j < index+1; j++) {
        char value = cards[1*size + j];
        printf("\narvo on %c\n", value);  //indekseja tarkistellaan liikaa
        switch(value)
        {
            case('A'):
                //Lisää ehto ynnätäänkö 1 vai 11;
                handValue += 11;
                break;
            case('T'):
                handValue += 10;
                break;
            case('J'):
                handValue += 10;
                break;
            case('Q'):
                handValue += 10;
                break;
            case('K'):
                handValue += 10;
                break;
            default:
                //muunna char -> int (onko oikein?)
                handValue += (int)value;
                break;
        }

    }
    return handValue;
}