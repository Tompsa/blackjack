/* cardfuncs.c */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "cardfuncs.h"

void createDeck(Deck *deck)
{
  Card *newCard = NULL;
  Card *nextCard = NULL;
  Card *prevCard = NULL;
  short int index = -1;
  for(enum cardSuit i = CLUBS; i <= SPADES; i++)
    {
      for(short int j = 1; j <= 13; j++)
        {
	  newCard = (Card*) malloc(sizeof(Card));
	  index++;
	  newCard->index = index;
	  newCard->suit = i;
	  newCard->value = j;
	  newCard->next = NULL;
	  if(index == 0)
	    deck->head = newCard;
	  prevCard = scanCards(deck->head, newCard->index);
	  nextCard = prevCard->next;
	  if((nextCard == NULL) || (nextCard->index != newCard->index))
            {
	      prevCard->next = newCard;
	      newCard->next = nextCard;
            }
        }
    }
}

void insertCard(Card *card, Card *newCard)
{
  newCard->next = card->next;
  card->next = newCard;
}

void shuffleDeck(Deck *deck)
{
  /*Initialize random seed*/
  time_t timer;
  srand((unsigned int) time(&timer));
  Card *cards[52];
  Card *current;
  current = deck->head;
  /*Create list of card pointers*/
  for(short int i = 0; i < 52; i++)
    {
      cards[i] = current;
      current = current->next;
    }
  /*Shuffle pointers*/
  for(short int i = 51; i >= 1; i--)
    {
      short int j = rand()%52;
      Card *temp = cards[i];
      cards[i] = cards[j];
      cards[j] = temp;
    }
  /*Rebuild deck*/
  current = cards[0];
  cards[51]->next = NULL;
  deck->head = current;
  for(short int i = 1; i < 52; i++)
    {
      current->next = cards[i]; 
      current = current->next;
    }
}

Card *scanCards(Card *head, int searchIndex)
{
  Card *previous = NULL; 
  Card *current = NULL;
  previous = head;
  current = head->next;
  while((current != NULL) && (current->index < searchIndex))
    {
      previous = current;
      current = current->next;
    }
  return previous;
}

void printCards(Deck *deck, int index)
{
  int i = 1;  
  Card  *current = NULL;
  current = deck->head;
  while((current != NULL))
    {
      switch(current->suit){
      case CLUBS:
	printf("Clubs");
	break;
      case DIAMONDS:
	printf("Diamonds");
	break;
      case HEARTS:
	printf("Hearts");
	break;
      case SPADES:
	printf("Spades");
	break;
      default:
	printf("Error");
	break;
      }
      printf(" %d ", current->value);
      if(index != 0)
	{
	  if(i == index)
	    break;
	  else
	    i++;
	}
      current = current->next;
    }
}

void dealCard(Deck *deck, Deck *hand)
{
  Card *deckCurrent = NULL;
  Card *deckNext = NULL;
  Card *handCurrent = NULL;
  Card *handNext = NULL;
  handCurrent = hand->head;
  deckCurrent = deck->head;
  deckNext = deckCurrent->next;

  if(handCurrent == NULL)
    {
      handCurrent = deckCurrent;
      handCurrent->next = NULL;
      hand->head = handCurrent;
    }
  else
    {
      handNext = handCurrent->next;
      while(handNext != NULL)
	{
	  handCurrent = handNext;
	  handNext = handCurrent->next;
	}
      insertCard(handCurrent, deckCurrent);
    }
  deck->head = deckNext;
}

short int getHandValue(Deck *hand)
{
  short int totalValue = 0;
  Card *current = NULL;
  Card *previous = NULL;
  current = hand->head;
  while(current != NULL)
    {
      previous = current;
      current = current->next;
      totalValue += previous->value;
    }
  return totalValue;
}

void cleanCards(Deck *deck)
{
  Card *current = NULL;
  Card *next = NULL;
  current = deck->head;
  while(current != NULL)
    {
      next = current->next;
      free(current);
      current = next;
    }
}
