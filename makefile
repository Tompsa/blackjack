# http://mrbook.org/blog/tutorials/make/

# Compiler to be used
CC=gcc

# Flags passed to compiler
CFLAGS=-c -g -Wall -Wextra -std=c99

#Source files
SOURCES=gamelogic.c cardfuncs.c

#Object files
OBJECTS=$(SOURCES:.c=.o)

EXE=blackjack

all: $(SOURCES) $(EXE)

$(EXE): $(OBJECTS) ; $(CC) $(OBJECTS) -o $@
    
.c.o: ; $(CC) $(CFLAGS) $< -o $@   
    
clean: ; rm -f $(OBJECTS)
